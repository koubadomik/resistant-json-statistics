import pandas as pd
import random


def get_hist_data():
    return [
        (
            (
                "state",
                [
                    {
                        "state": "Florida",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Florida",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Ohio",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Ohio",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Kendrick",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Kendrick",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                ],
            ),
            [("Florida", 2), ("Ohio", 2), ("Kendrick", 2)],
        ),
        (
            (
                "state",
                [
                    {
                        "state": "",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": None,
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "Ohio",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": "",
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                ],
            ),
            [("", 3), ("None", 2), ("Ohio", 1)],
        ),
    ]


def get_array_hist_data():
    return [
        (
            (
                "state",
                [
                    {
                        "state": ["Florida", "Ohio"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Kansas"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Horo", "Ohio"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Ohio", "Kansas"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Kendrick"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Florida"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                ],
            ),
            [("Florida", 2), ("Ohio", 3), ("Kendrick", 1), ("Kansas", 2), ("Horo", 1)],
        ),
        (
            (
                "state",
                [
                    {
                        "state": ["Florida", None],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": [""],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Horo", "Ohio"],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": ["Ohio", ""],
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "state": None,
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                    {
                        "shortname": "FL",
                        "info": {"governor": "Rick Scott"},
                        "counties": [
                            {"name": "Dade", "population": 12345},
                            {"name": "Broward", "population": 40000},
                            {"name": "Palm Beach", "population": 60000},
                        ],
                    },
                ],
            ),
            [("Florida", 1), ("Ohio", 2), ("None", 2), ("", 2), ("Horo", 1)],
        ),
    ]


def get_aggregations_data():
    return [
        (("mean", pd.DataFrame({"column": [12.4, 54.3, 3.8]})), 23.5),
        (("max", pd.DataFrame({"column": [12.4, 54.3, 3]})), 54.3),
        (("median", pd.DataFrame({"column": [12.4, 54.3, 3.8]})), 12.4),
        (("p90", pd.DataFrame({"column": [12.4, 54.3, 3.8]})), 45.92),
        (("mean", pd.DataFrame({"column": [12.4, 54.3, 3.8, None]})), 23.5),
    ]
