import json
import src.io.files as files


def test_read_filter_csv_files_givenEmptyArray_expectEmptyArray():
    expected = []
    given = files.read_filter_csv_files([], 1)
    assert sorted(given) == sorted(expected)


def test_read_filter_csv_files_givenArrayOfFiles_expectProperArrayOfJsons():
    expected = [{"valid": "json"}, {"valid": "json"}]
    expected = json.dumps(expected, sort_keys=True)
    fils = [
        "./tests/data/valid.csv",
        "./tests/data/invalid.csv",
        "tests/data/nonexistent.csv",
    ]

    given = files.read_filter_csv_files(fils, 2)
    given = json.dumps(given, sort_keys=True)

    assert given == expected


def test_csv_to_array_givenValidCsv_expectArrayOfArrays():
    expected = [
        ["Hello World", "Nothing"],
        ["Hello World", '{"valid":"json"}'],
        ["Hello World", "Nothing"],
        ["Hello World", '{"valid":"json"}'],
    ]

    file = "./tests/data/valid.csv"
    given = files.csv_to_array(file)
    assert sorted(given) == sorted(expected)


def test_csv_to_array_givenNonexistentFile_expectEmptyArray():
    expected = []

    file = "./data/nonexistent.csv"
    given = files.csv_to_array(file)
    assert sorted(given) == sorted(expected)


def test_csv_to_array_givenCsvWithVariousColumnNumber_expectValidArray():
    expected = [["gjreigeo", "rjghiuoege", "jriegjoi"], ["grjeio", "rehgo"]]

    file = "./tests/data/invalid.csv"
    given = files.csv_to_array(file)
    assert sorted(given) == sorted(expected)


def test_filter_valid_format_data_givenValidarrayOfStrings_expectArrayOfDictionariesofSpecifiedData():
    data = [
        ["Hello World", "Nothing"],
        ["Hello World", '{"valid": "json"}'],
        ["Hello World", "Nothing"],
        ["Hello World", '{"valid": "json", "hurrray" : "hello"}'],
    ]

    expected = [{"valid": "json"}, {"valid": "json", "hurrray": "hello"}]
    expected = json.dumps(expected, sort_keys=True)

    given = files.filter_valid_format_data(data, 2)
    given = json.dumps(given, sort_keys=True)

    assert given == expected


def test_filter_valid_format_data_givenValidarrayNotJsonOfStrings_expectEmptyArray():
    data = [["Hello World", "Nothing"], ["Hello World", "Nothing"]]

    expected = []

    given = files.filter_valid_format_data(data, 2)

    assert given == expected
