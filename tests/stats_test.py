import pytest
from src.stats.stats import hist, array_hist, aggregations_for_numbers
import tests.data.stats as data
import pandas as pd

HIST_DATA = data.get_hist_data()
ARRAY_HIST_DATA = data.get_array_hist_data()
AGGREGATION_DATA = data.get_aggregations_data()


@pytest.mark.parametrize("test_input, expected", HIST_DATA)
def test_hist(test_input, expected):
    data = pd.json_normalize(test_input[1])
    expected.sort(key=lambda x: (x[1], x[0]), reverse=True)
    field = test_input[0]
    result = hist(field, data)[1]
    assert result == expected


@pytest.mark.parametrize("test_input, expected", ARRAY_HIST_DATA)
def test_array_hist(test_input, expected):
    data = pd.json_normalize(test_input[1])
    expected = sorted(expected, key=lambda x: (x[1], x[0]), reverse=True)
    field = test_input[0]
    result = array_hist(field, data)[1]
    assert result == expected


@pytest.mark.parametrize("test_input, expected", AGGREGATION_DATA)
def test_aggregations(test_input, expected):
    df = test_input[1]
    item = test_input[0]
    result = aggregations_for_numbers(df)
    assert result[item] == expected


# aggregation for numbers
# check right results
# what if not presented in data frame
# what if no number values
# what if array value or not number
# what nones are doing (pandas defaults to skip)
