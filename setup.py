from setuptools import setup, find_packages


setup(
    name="resistant-json-statistics",
    version="0.1.0",
    packages=find_packages(),
)
