# resistant-json-statistics

# Download and run
- `git clone git@gitlab.com:koubadomik/resistant-json-statistics.git`
- `cd resistant-json-statistics`
- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip3 install -r requirements.txt`
- `python3 rjs.py --help`

# Output of rjs.py - based on requirements
Report is dumped to stdout in json format divided into
three basic groups - each file_type and one overall. There are statistics for each 
group separately in the report, it contains following:
- count of document of current file_type
- sorted histograms for status_code, score, visual_class and indicator_ids
- numerical statistics for processing, detection time and their sum

# Run tests
- install project
- run `pytest` from root directory
