import pandas as pd
from src.utils.utils import flatten


def get_jobs():
    """Returns array of functions which accepts dataframe and return
    tuple (name, dictionary), where `name` represents name of statistic
    and `dictionary` statistic result in structured form.

    In case of new statistic extension, we have to add function to the following array,
    to be called. Interface is strictly:
        (data:pd.dataframe) --> (name:string, statistic:dictionary)
    """
    return [
        hist_func("status_code"),
        hist_func("message.report.score"),
        hist_func("message.report.visual_class"),
        array_hist_func("message.report.indicator_ids", none=False),
        num_statistics,
    ]


def hist(col, data):
    """Given `data:pd.dataframe` and `col` representing column name return
    histogram of values in this column as tuple:
        (``col` histogram`:string,
         histogram:array of arrays sorted according to frequency (desc))
    Column has be represented as atomic string values not array.
    None values and empty values are counted as same thing and in result are
    presented as `None` item.
    """
    data = data.fillna("None")
    grouped = data.groupby(by=col, dropna=False)
    result = [
        (name, count)
        for name, count in zip(list(grouped.groups.keys()), grouped.size().tolist())
    ]
    result.sort(key=lambda x: (x[1], x[0]), reverse=True)
    return ("{}_histogram".format(col), result)


def array_hist(col, data, none=True):
    """Function does exactly the same as hist(col, data), but column
    has to contain only lists of strings. None values in particular arrrays are skipped,
    None values out of it (atomic) are reflected in result if `none`.
    """
    data = data.fillna("None")
    column = data[col]
    values = list(set(flatten(column.tolist())))
    if None in values:
        values.remove(None)  # because of None values in particular arrays
    if not none and "None" in values:
        values.remove("None")
    counts = []
    for value in values:
        counts.append(
            (
                value,
                sum(
                    column.apply(
                        lambda x: value in x if isinstance(x, list) else value in [x]
                    )
                ),
            )
        )
    results = sorted(counts, key=lambda x: (x[1], x[0]), reverse=True)
    return ("{}_histogram".format(col), results)


def aggregations_for_numbers(data):
    """Given `data:pd.dataframe` returns mean,min,max,median,p90,p95,p99 of
    all number values in dataframe. Return type is dictionary where key is name
    of statistic and value its result. None values are skipped, other types than
    double, integer and float are prohibited.
    """
    return {
        "mean": float(data[data.notnull()].mean()),
        "min": float(data[data.notnull()].min()),
        "max": float(data[data.notnull()].max()),
        "median": float(data[data.notnull()].median()),
        "p90": float(data[data.notnull()].quantile(0.9)),
        "p95": float(data[data.notnull()].quantile(0.95)),
        "p99": float(data[data.notnull()].quantile(0.99)),
    }


def hist_func(col):
    """Given `col` (name of column) returns function accepting data:pd.dataframe
    and outputing histogram of given column.
    """
    return lambda data: hist(col, data)


def array_hist_func(col, none=True):
    """Given `col` (name of column) returns function accepting data:pd.dataframe
    and outputing array histogram of given column.
    """
    return lambda data: array_hist(col, data, none)


def num_statistics(data):
    return (
        "time_statistics",
        {
            "preprocessing_time": aggregations_for_numbers(
                data["message.preprocessing_time"]
            ),
            "detection_time": aggregations_for_numbers(data["message.detection_time"]),
            "preprrocessing_detection_time": aggregations_for_numbers(
                data["message.preprocessing_time"] + data["message.detection_time"]
            ),
        },
    )


def common_hist(col, data):
    """Given `col:string` as column name and `data:pd.dataframe`
    returns histogram of given column based on type of column atomic_hist
    or array_hist is called .
    """
    # TODO later could be better, now not necessary
    pass
