import src.stats.stats as stats
import pandas as pd


def get_stats(data):
    """Given `data:pd.dataframe` returns dictionary containing statistics."""
    if not len(data):
        return {}
    subsets = get_subsets(data, "file_type")
    subsets = [("overall", data)] + subsets  # we want stats even for the whole df
    jobs = stats.get_jobs()
    result = perform_stats(subsets, jobs)
    return result


def perform_stats(subsets, jobs):
    """Given `subsets` as an array of tuples in format
    (name_of_subset:string, data:pd.dataframe) and jobs as an array of functions
    returns statistics for each group, all in one dictionary.
    """
    statistics = {}
    for subset in subsets:
        statistics[subset[0]] = {}
        statistics[subset[0]]["count"] = len(subset[1])
        statistics[subset[0]]["result"] = {}
        for job in jobs:
            name, result = job(subset[1])
            statistics[subset[0]]["result"][name] = result
    return statistics


def get_subsets(data, col):
    """Given data:pd.dataframe returns subsets as an array of tuples where each item has
    following format: (name_of_subset:string, data:pd.dataframe).
    Grouping is performed based on `col:string` column name, it returns
    subset for each group. None is not taken as valid group. Raises exception
    if `col` is not presented in dataframe
    """
    types = data[col].unique().tolist()
    result = [
        (
            type,
            (data[data[col] == type]),
        )
        for type in types
        if type is not None
    ]
    return result
