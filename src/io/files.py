import csv
import json
import os
import logging
from src.utils.utils import flatten


def read_filter_csv_files(files, report_col, header=True, delimiter=","):
    """Given list of csv files (path format) returns list of dictonaries
    representing log reports which are parsed from `report_col`
    (int representing place of the column in order, starting from 1).
    Unless csv file contains exactly `column_number` columns, it is skipped.
    """
    data = flatten([csv_to_array(file, header, delimiter) for file in files])
    data = filter_valid_format_data(data, report_col)
    return data


def csv_to_array(file, header=True, delimiter=","):
    """Given path to csv `file` returns parsed rows as an array of arrays, each
    array representing row, all items in particular array are strings. Invalid
    csv regarding various number of columns is also accepted and parsed.
    """
    data = []
    try:
        with open(file, "r") as f:
            reader = csv.reader(f, delimiter=delimiter)
            if header:
                next(reader)
            for row in reader:
                data.append(row)
    except FileNotFoundError:
        logging.info("File {} not found".format(file))
        return []
    return data


def filter_valid_format_data(data, col):
    """Given data as array of arrays of strings representing rows in table
    returns json parsable strings in `col`
    (int representing place in order of columns starting from 1, not larger than number of columns)
    as array of dictionaries. Not valid json strings in column are skipped as well
    as rows with less columns than `col` (to be robust).
    """
    col = col - 1  # index in array
    filtered = []
    for row in data:
        if len(row) > col:
            try:
                data = json.loads(row[col])
                filtered.append(data)
            except ValueError as e:
                # we want only valid jsons, silently skip
                continue
    return filtered
