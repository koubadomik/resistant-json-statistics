import json
import re
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import TerminalFormatter
import pandas as pd


def flatten(array):
    """Returns 1D array containing all elements in 2D `array`. `array`
    could contain even atomic values"""
    arrays = [x for x in array if isinstance(x, list)]
    atomic = [x for x in array if not isinstance(x, list)]
    result = [item for sublist in arrays for item in sublist]
    return result + atomic


def pretty_json_print(dictionary):
    """Given python dictionary returns colorful json string
    to be printed in terminal
    """
    json_str = json.dumps(dictionary, indent=4)
    json_str = re.sub(r'",\s+', '", ', json_str)
    return highlight(json_str, JsonLexer(), TerminalFormatter())


def dict_arr_to_pd_df(array):
    """Given array of python dictionaries return pandas dataframe. Each json
    is one row in resulting dataframe, dict is flatten.
    """
    return pd.json_normalize(array)
