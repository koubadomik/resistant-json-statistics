import click
import logging
from src.io.files import read_filter_csv_files
from src.stats.analyse import get_stats
from src.utils.utils import pretty_json_print, dict_arr_to_pd_df

logging.basicConfig(level=logging.INFO, format="%(message)s")


@click.command()
@click.argument("src", nargs=-1, required=True, type=click.Path())
def display_statistics(src):
    """This script will output predefined statistics for given
    lambda output log document/s (SRC). Output is in JSON format, INPUT
    files should be CSV containing two columns (timestamp:string, log:string).
    Second column contains json string which the statistics is calulated on.
    This column does not have to contain only json strings.
    """
    jsons = read_filter_csv_files(src, 2)
    df = dict_arr_to_pd_df(jsons)
    result = get_stats(df)
    click.echo(pretty_json_print(result))


if __name__ == "__main__":
    display_statistics()
